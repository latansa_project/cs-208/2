-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 25, 2024 at 02:24 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpemesanan1`
--

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int NOT NULL,
  `tanggal_pemesanan` date NOT NULL,
  `total_belanja` int NOT NULL,
  `kode_pesanan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `tanggal_pemesanan`, `total_belanja`, `kode_pesanan`) VALUES
(64, '2024-05-19', 24000, 'KD-275'),
(65, '2024-05-19', 24000, 'KD-316'),
(66, '2024-05-19', 21000, 'KD-770');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan_produk`
--

CREATE TABLE `pemesanan_produk` (
  `id_pemesanan_produk` int NOT NULL,
  `id_pemesanan` int NOT NULL,
  `id_menu` varchar(50) NOT NULL,
  `jumlah` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `pemesanan_produk`
--

INSERT INTO `pemesanan_produk` (`id_pemesanan_produk`, `id_pemesanan`, `id_menu`, `jumlah`) VALUES
(49, 57, '7', 4),
(50, 57, '8', 2),
(51, 58, '9', 1),
(52, 59, '8', 2),
(53, 59, '6', 1),
(54, 59, '12', 2),
(55, 59, '17', 2),
(56, 60, '7', 3),
(57, 60, '16', 1),
(58, 61, '11', 1),
(59, 62, '7', 2),
(60, 62, '9', 3),
(61, 63, '6', 2),
(62, 64, '6', 2),
(63, 65, '6', 2),
(64, 66, '10', 3);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_menu` int NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `jenis_menu` varchar(50) NOT NULL,
  `stok` int NOT NULL,
  `harga` int NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_menu`, `nama_menu`, `jenis_menu`, `stok`, `harga`, `gambar`) VALUES
(6, 'RED VELVET', 'Minuman', 3, 12000, 'redvelvet.jpg'),
(7, 'BLUE OCEAN', 'Minuman', 100, 10000, 'cocktail.jpg'),
(8, 'CHOCO CLASSIC', 'Minuman', 100, 12000, 'coklat.jpg'),
(9, 'CHOCO HAZELNUT', 'Minuman', 100, 14000, 'bobamilk.jpg'),
(10, 'KOPI TUBRUK', 'Minuman', 5, 7000, '34f56cb1-d6f7-4ddd-b0e9-ec36b94bbfdb.jpeg'),
(11, 'MATCHA', '', 2, 14000, 'MACHA.jpg'),
(12, 'TARO', 'Minuman', 100, 12000, '689a4885-53cc-4712-a3bf-e5d1613b9985.jpeg'),
(13, 'TAHU WALIK', 'Makanan', 55, 10000, '3bfe247f-2b75-4f86-960f-e1043a848ea8.jpeg'),
(14, 'GETUK GORENG ', 'Makanan', 50, 9000, '0c89bb85-fc78-4219-a44a-52ce6f474b39.jpeg'),
(15, 'MIE GORENG', 'Makanan', 50, 7000, '65ed3199-540f-4bbe-bdbb-950cfe77876f.jpeg'),
(16, 'ICE TEA', '', 120, 4000, 'tehobeng.jpg'),
(17, 'Air Mineral', 'Minuman', 100, 4000, 'sanford.jpg'),
(21, 'MIE REBUS', 'Makanan', 70, 7000, '8356e8ed-f101-4b7b-90d3-c8f42a312af9.jpeg'),
(22, 'mie', 'Makanan', 20, 10, '8356e8ed-f101-4b7b-90d3-c8f42a312af9.jpeg'),
(23, 'GETUK GORENG', 'Makanan', 70, 12000, '0c89bb85-fc78-4219-a44a-52ce6f474b39.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama_lengkap` varchar(25) NOT NULL,
  `jenis_kelamin` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `hp` varchar(25) NOT NULL,
  `status` enum('admin','user','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama_lengkap`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `hp`, `status`) VALUES
(1, 'daus', 'daus123', 'Alfirdaus Muhammad Farhan', 'Laki-Laki', '1998-05-14', 'Tanjung Piayu', '089560328673', 'admin'),
(2, 'rinaldo', 'rinaldo123', 'Rinaldo', 'Laki-Laki', '1999-01-11', 'Tanjung Uma', '085233748222', 'user'),
(3, 'admin', 'admin', 'Alfirdaus Muhammad Farhan', 'Laki-Laki', '1998-05-19', 'Tanjung Piayu', '089123614882', 'admin'),
(4, 'user', 'user', 'Rinaldo', 'Laki-Laki', '1998-10-22', 'Tanjung Uma', '089560328673', 'user'),
(5, 'rinaldo', 'rinaldo', 'Rinaldo', 'Laki-Laki', '1999-02-23', 'Tanjung Uma', '089123614882', 'user'),
(6, 'daus', 'daus123', 'Alfirdaus Muhammad Farhan', 'Laki-Laki', '1998-05-14', 'Tanjung Piayu', '085233748222', 'admin'),
(7, 'aji', 'aji123', 'nugroho aji', 'Laki-Laki', '2022-07-31', 'jl anggrek desa sari kec.', '0866675432', 'admin'),
(8, 'bima', 'bima123', 'bima', 'Laki-Laki', '2002-02-01', 'gang jambu', '0876453218', 'user'),
(9, 'andi', 'andi123', 'andi petruk', 'Laki-Laki', '0000-00-00', '', '', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `pemesanan_produk`
--
ALTER TABLE `pemesanan_produk`
  ADD PRIMARY KEY (`id_pemesanan_produk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `pemesanan_produk`
--
ALTER TABLE `pemesanan_produk`
  MODIFY `id_pemesanan_produk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_menu` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
